import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_tasks import delay_task, run_task, create_contract_in_opencell


class RunTasksTests(unittest.TestCase):

    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.OpenCellConfiguration.sync_to_opencell", new_callable=mock.PropertyMock, return_value=False)
    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.create_contract_in_opencell")
    def test_run_task_runs_nothing_if_sync_to_opencell_disabled(self, create_contract_in_opencell_mock, _):
        run_task(create_contract_in_opencell, mock.ANY)
        create_contract_in_opencell_mock.assert_not_called()

    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.OpenCellConfiguration.sync_to_opencell", new_callable=mock.PropertyMock, return_value=True)
    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.create_contract_in_opencell")
    def test_run_task_runs_nothing_if_sync_to_opencell_disabled(self, create_contract_in_opencell_mock, _):
        run_task(create_contract_in_opencell_mock, mock.ANY)
        create_contract_in_opencell_mock.assert_called_with(mock.ANY)


class DelayTasksTests(unittest.TestCase):

    @unittest.skip("async task disabled by now")
    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.OpenCellConfiguration.sync_to_opencell", new_callable=mock.PropertyMock, return_value=False)
    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.create_contract_in_opencell.apply_async")
    def test_delay_task_runs_nothing_if_sync_to_opencell_disabled(self, create_contract_in_opencell_async_mock, _):
        delay_task(create_contract_in_opencell, mock.ANY)
        create_contract_in_opencell_async_mock.assert_not_called()

    @unittest.skip("async task disabled by now")
    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.OpenCellConfiguration.sync_to_opencell", new_callable=mock.PropertyMock, return_value=True)
    @mock.patch("opencell_somconnexio.tryton_tasks.tasks.create_contract_in_opencell.apply_async")
    def test_delay_task_runs_nothing_if_sync_to_opencell_disabled(self, create_contract_in_opencell_async_mock, _):
        delay_task(create_contract_in_opencell, mock.ANY)
        create_contract_in_opencell_async_mock.assert_called_with(
            args=(mock.ANY,),
            kwargs={},
            countdown=30,
        )


class CreateContractInOpenCellTaskTests(unittest.TestCase):

    def test_create_contract_in_opencell_is_a_celery_task(self):
        self.assertTrue(hasattr(create_contract_in_opencell, "apply_async"))

    # TODO - Test that `create_contract_in_opencell` task calls `create_contract` action.
    # We want to test that action `create_contract` is called, but we got stuck in a issue that is
    # very time consuming
