import unittest2 as unittest
from opencell_somconnexio.opencell_models.opencell_types.custom_field import CustomField


class OpenCellCustomFieldTypeTests(unittest.TestCase):

    def test_dict_format(self):
        expected_custom_field_dict = {
            "code": "CustomFieldA",
            "fieldType": "STRING",
            "stringValue": "1234"
        }

        cf = CustomField(
            code="CustomFieldA",
            value="1234")

        self.assertEqual(expected_custom_field_dict, cf.to_dict())
