import mock
import unittest2 as unittest
from datetime import datetime

from .factories import AccountAccountFactory, AccountTaxFactory, CompanyFactory, JournalFactory, PartyFactory, \
    OpenCellInvoiceFactory, PeriodFactory
from .opencell_factories import OpenCellRawInvoiceFactory
from .pool_mock_proxy import PoolMockProxy


@mock.patch("opencell_somconnexio.services.account_move_creator.Pool", return_value=PoolMockProxy)
class AccountMoveCreatorServiceTest(unittest.TestCase):
    """ Test the AccountMoveCreatorService. """

    def _journal(self):
        self.expected_journal = JournalFactory()
        PoolMockProxy.AccountJournalMock = mock.Mock(spec=["search"])

        # We use this journal_side_effect fuction to check if the values passed to the search are the expected
        # https://stackoverflow.com/questions/27209701/python-mock-function-with-only-specific-argument
        def journal_side_effect(value):
            if value == [('code', '=', 'REV')]:
                return [self.expected_journal]
            else:
                return None
        PoolMockProxy.AccountJournalMock.search.side_effect = journal_side_effect

    def _party(self):
        self.party = PartyFactory()

        def party_side_effect(value):
            if int(value) == self.party.id:
                return self.party
            else:
                return None
        PoolMockProxy.PartyPartyMock = mock.Mock(side_effect=party_side_effect)

    def _company(self):
        self.expected_company = CompanyFactory()

        def company_side_effect(company_id):
            if company_id == 1:
                return self.expected_company

        PoolMockProxy.CompanyCompanyMock = mock.Mock(side_effect=company_side_effect)

    def _account(self):
        self.account_client = AccountAccountFactory(code="43000000")
        self.account_taxes = AccountAccountFactory(code="70500020")
        self.account_service = AccountAccountFactory(code="47700021")

        def account_side_effect(value):
            if value == [('code', '=', '43000000')]:
                return [self.account_client]
            elif value == [('code', '=', '70500020')]:
                return [self.account_service]
            elif value == [('code', '=', '47700021')]:
                return [self.account_service]
        PoolMockProxy.AccountAccountMock = mock.Mock(spec=["search"])
        PoolMockProxy.AccountAccountMock.search.side_effect = account_side_effect

    def _opencell_tax_codes(self):
        self.account_tax = AccountTaxFactory(invoice_account=self.account_taxes)

        def account_tax_side_effect(value):
            if value == "TAX_HIGH":
                return self.account_tax
        PoolMockProxy.OpenCellTaxCodesMock = mock.Mock(spec=["get_tax_for_opencell_code"])
        PoolMockProxy.OpenCellTaxCodesMock.get_tax_for_opencell_code.side_effect = account_tax_side_effect

    def _period(self):
        self.expected_period = PeriodFactory()

        invoice_date = datetime.strptime(self.invoice_date, '%Y-%m-%d').date()

        def period_side_effect(value):
            if value == [('start_date', '<=', invoice_date), ('end_date', '>=', invoice_date)]:
                return [self.expected_period]
            return []

        PoolMockProxy.PeriodMock = mock.Mock(spec=["search"])
        PoolMockProxy.PeriodMock.search.side_effect = period_side_effect

    def setUp(self):
        self._account()
        self._company()
        self._journal()
        self._party()
        self._opencell_tax_codes()

        invoice_id = 27
        invoice_number = 44

        self.invoice_date = "2019-04-30"
        self.opencell_raw_invoice = OpenCellRawInvoiceFactory(
            invoice_id, invoice_number, self.party.id, self.invoice_date)
        self.opencell_invoice = OpenCellInvoiceFactory(
            invoice_id, invoice_number, datetime.strptime(self.invoice_date, "%Y-%m-%d").date())

        self._period()
