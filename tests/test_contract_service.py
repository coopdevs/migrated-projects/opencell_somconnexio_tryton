import mock
import unittest2 as unittest
from opencell_somconnexio.services.contract import ContractService
from opencell_somconnexio.opencell_models.crm_account_hierarchy import CRMAccountHierarchyFromContract

from .factories import ContractFactory


class SubscriptionFake:
    userAccount = 1234


class ContractServiceTests(unittest.TestCase):

    def setUp(self):
        self.contract = ContractFactory()
        self.opencell_configuration = mock.Mock()

    @staticmethod
    def mock_subscription_get():
        return SubscriptionFake()

    @mock.patch("opencell_somconnexio.services.contract.SubscriptionService.get", mock_subscription_get)
    @mock.patch("opencell_somconnexio.services.contract.CRMAccountHierarchy")
    def test_contract_service_update(self, CRMAccountHierarchyMock):
        """ Call to CRMAccountHierarchy when call to update of ContractService """
        subscription = SubscriptionFake()

        crm_account_hierarchy_from_contract = CRMAccountHierarchyFromContract(self.contract, subscription.userAccount)

        ContractService(self.contract, self.opencell_configuration).update()
        CRMAccountHierarchyMock.update.assert_called_with(**crm_account_hierarchy_from_contract.to_dict())
