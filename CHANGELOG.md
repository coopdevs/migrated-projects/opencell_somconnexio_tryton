# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [1.4.0] - 2020-09-07
### Added

- Add service address to broadband subscription [#95](https://gitlab.com/coopdevs/opencell_somconnexio_tryton/-/merge_requests/95)

## [1.3.0] - 2020-04-28
### Added

- Track errors in Bugsnag [#92](https://gitlab.com/coopdevs/opencell_somconnexio_tryton/-/merge_requests/92) and [#94](https://gitlab.com/coopdevs/opencell_somconnexio_tryton/-/merge_requests/94)

## [v1.2.6] - 2019-11-22
