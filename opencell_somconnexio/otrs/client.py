import os

from pyotrs import Client
from pyotrs.lib import APIError, HTTPError, ResponseParseError, SessionNotCreated, ArgumentMissingError


class OTRSClientException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)
        self.message = message


class ErrorCreatingSession(OTRSClientException):
    pass


class TicketNotCreated(OTRSClientException):
    pass


class OTRSClient(object):
    def __init__(self):
        self.client = Client(
            baseurl=os.environ.get('OTRS_URL'),
            username=os.environ.get('OTRS_USER'),
            password=os.environ.get('OTRS_PASSW'))

    def _create_session(self):
        """ Create a OTRS Client with session open to play calls.

        This method call to the OTRS API to create a session to play another requests with authentication done.
        Raise User errors to show the problem with the request if it is fault.
        """
        try:
            self.client.session_create()
        except (HTTPError, APIError, ResponseParseError) as error:
            raise ErrorCreatingSession(error.message)

    def create_ticket(self, ticket, article):
        """ Create a OTRS Process Ticket to manage the provisioning.

        This method call to the OTRS API to create a ticket with all the information of the econtract.
        If the Ticket is created, return the response to save the ID and the number in the EticomContract
        model to keep the relation between systems.
        Else, raise an error with the needed information to fix the EticomContract and rerun the process.
        """
        self._create_session()
        try:
            self.client.ticket_create(ticket=ticket, article=article)
        except (HTTPError, APIError, ResponseParseError, SessionNotCreated, ArgumentMissingError) as error:
            raise TicketNotCreated(error.message)
