from .opencell_resource import OpenCellResource
from .opencell_types.description import Description
from .opencell_types.address import Address


class AccountHierarchyResource(OpenCellResource):
    MAX_LENGTH = 50

    @property
    def name(self):
        # DTO in OC - https://api.opencellsoft.com/6.0.0/json_NameDto.html
        first_name = self.party.first_name or ""
        last_name = self.party.name or ""
        return {
            "firstName": first_name[:self.MAX_LENGTH],
            "lastName": last_name[:self.MAX_LENGTH],
        }

    @property
    def description(self):
        return Description(self.party.full_name).text

    @property
    def address(self):
        contact_address = self.party.get_contact_address()
        if not contact_address:
            contact_address = self.party.addresses[0]
        return Address(
            address=contact_address.street,
            zip=contact_address.zip,
            city=contact_address.city,
            state=contact_address.subdivision.name,
            country=contact_address.country.code).to_dict()

    @property
    def vatNo(self):
        return self.party.vat_code

    @property
    def contactInformation(self):
        # DTO in OC - https://api.opencellsoft.com/6.0.0/json_ContactInformationDto.html
        return {
            "email": self.email,
            "mobile": self.phone,
        }
