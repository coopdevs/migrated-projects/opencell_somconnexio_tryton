from pyopencell.resources.subscription import Subscription

from ..exceptions import OpenCellConfigurationDoesNotExist
from ..opencell_models.services import ContractLineToOCServiceDict


class SubscriptionService:
    """ Model to execute the bussines logic of Som Connexio working with the Subscription model of PyOpenCell """

    def __init__(self, contract, opencell_configuration=None):
        self.contract = contract
        self.opencell_configuration = opencell_configuration

    def terminate(self):
        subscription = self.get()
        subscription.terminate(self.contract.end_date.strftime("%Y-%m-%d"))

    def create_service(self, contract_line, product_template_id):
        subscription = self.get()

        try:
            one_shot_charge_code = self.opencell_configuration.get_one_shot_charge_code_for_product_template(
                product_template_id)
        except OpenCellConfigurationDoesNotExist:
            one_shot_charge_code = None

        try:
            service_code = self.opencell_configuration.get_service_code_for_product_template(product_template_id)
        except OpenCellConfigurationDoesNotExist:
            service_code = None

        if one_shot_charge_code:
            subscription.applyOneShotCharge(one_shot_charge_code)
        elif service_code:
            opencell_service_dict = ContractLineToOCServiceDict(contract_line, self.opencell_configuration).convert()
            subscription.activate([opencell_service_dict])

    def get(self):
        subscription_response = Subscription.get(self.contract.id)
        return subscription_response.subscription

    def update_service_termination_date(self, product_template_id, termination_date):
        if not termination_date:
            return

        try:
            service_code = self.opencell_configuration.get_service_code_for_product_template(product_template_id)
        except OpenCellConfigurationDoesNotExist:
            return

        termination_date = termination_date.strftime("%Y-%m-%d")

        subscription = self.get()
        for service in subscription.services["serviceInstance"]:
            service_needs_update = service["code"] == service_code and not service.get("terminationDate")
            if service_needs_update:
                subscription.terminateServices(termination_date, [service["code"]])
