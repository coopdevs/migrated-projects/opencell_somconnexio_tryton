import logging
from pyopencell.resources.access import Access
from pyopencell.resources.crm_account_hierarchy import CRMAccountHierarchy
from pyopencell.resources.customer import Customer
from pyopencell.resources.subscription import Subscription

from ..opencell_models.access import AccessFromContract
from ..opencell_models.crm_account_hierarchy import CRMAccountHierarchyFromContract
from ..opencell_models.customer import CustomerFromParty
from ..opencell_models.services import ServicesFromContract
from ..opencell_models.subscription import SubscriptionFromContract

from ..crm_account_hierarchy_strategies import CRMAccountHierarchyStrategies

from .subscription import SubscriptionService

logger = logging.getLogger(__name__)


class CRMAccountHierarchyFromContractService(object):
    def __init__(self, contract, opencell_configuration):
        self.contract = contract
        self.party = contract.party
        self.strategy_actions = {
            'customer_hierarchy': self._create_customer_subscription,
            'customer_account_hierarchy': self._create_subscription_from_crm_account_hierarchy,
            "subscription": self._create_subscription
        }
        self.opencell_configuration = opencell_configuration

    def run(self):
        if self.contract.state != 'confirmed':
            logger.warning("An attempt to create an unconfirmed contract (id={}) in opencell has been ignored".format(
                self.contract.id)
            )
            return
        strategy, kwargs = CRMAccountHierarchyStrategies(self.contract).strategies()
        self.strategy_actions[strategy](**kwargs)

    def _create_customer_subscription(self, crm_account_hierarchy_code):
        self._create_customer()
        self._create_subscription_from_crm_account_hierarchy(crm_account_hierarchy_code)

    def _create_subscription_from_crm_account_hierarchy(self, crm_account_hierarchy_code):
        crm_account_hierarchy_code = self._create_crm_account_hierarchy(crm_account_hierarchy_code)
        self._create_subscription(crm_account_hierarchy_code=crm_account_hierarchy_code)

    def _create_customer(self):
        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)
        Customer.create(**customer_from_party.to_dict())

    def _create_crm_account_hierarchy(self, crm_account_hierarchy_code):
        crm_account_hierarchy_from_contract = CRMAccountHierarchyFromContract(self.contract, crm_account_hierarchy_code)
        CRMAccountHierarchy.create(**crm_account_hierarchy_from_contract.to_dict())
        return crm_account_hierarchy_from_contract.code

    def _create_subscription(self, crm_account_hierarchy_code):
        subscription_from_contract = SubscriptionFromContract(self.contract, crm_account_hierarchy_code)
        Subscription.create(**subscription_from_contract.to_dict())

        self._create_access()

        services_to_activate = ServicesFromContract(self.contract, self.opencell_configuration).services_to_activate()
        if not services_to_activate:
            logger.error("Coulnd't activate subscription, no services suitable for activation found!")
            return

        subscription = SubscriptionService(self.contract, self.opencell_configuration).get()
        subscription.activate(services_to_activate)

    def _create_access(self):
        access_from_contract = AccessFromContract(self.contract)
        Access.create(**access_from_contract.to_dict())
