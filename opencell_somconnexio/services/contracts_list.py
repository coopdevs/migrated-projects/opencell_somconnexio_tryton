import logging

from pyopencell.exceptions import PyOpenCellAPIException, PyOpenCellHTTPException

from .contract import ContractService
from ..otrs.ticket import OTRSTicket, send_opencell_error_report


logger = logging.getLogger(__name__)


class ContractsListService:
    """
    Manage a list of ContractService and the bussines logic of Som Connexio working with them.

    """

    def __init__(self, contracts, opencell_configuration=None):
        self.contracts = contracts
        self.opencell_configuration = opencell_configuration

    def _is_email_updated(self):
        return bool(self.values.get('contact_email'))

    def _is_bank_account_updated(self):
        return bool(self.values.get('receivable_bank_account'))

    def _has_different_emails(self, all_party_contracts):
        current_email = self.contracts[0].contact_email.value
        for contract in all_party_contracts:
            if contract.contact_email.value != current_email:
                return True
        return False

    def _are_changing_all_contracts(self, all_party_contracts):
        return len(self.contracts) == len(all_party_contracts)

    def _create_tickets(self):
        if self._is_email_updated():
            ticket_type = u'canvi email'
        elif self._is_bank_account_updated():
            ticket_type = u'canvi IBAN'

        for contract in self.contracts:
            ticket_message = '{}: revisar jerarquia complexa: {}'.format(contract.party.id, ticket_type)
            OTRSTicket(contract, title=ticket_message, body=ticket_message).create()

    def _update_contract(self, contract):
        try:
            ContractService(contract, self.opencell_configuration).update()
        except (PyOpenCellHTTPException, PyOpenCellAPIException) as exc:
            send_opencell_error_report(contract.id, exc)

    def _update_email(self, all_party_contracts):
        email = self.values.get('contact_email')
        for contract in self.contracts:
            contract.contact_method = email
            self._update_contract(contract)

    def _update_bank_account(self, all_party_contracts):
        bank_account = self.values.get('receivable_bank_account')
        for contract in self.contracts:
            contract.receivable_bank_account = bank_account
            self._update_contract(contract)

    def update(self, values, all_party_contracts):
        """ This method read the values updated and select the task to run. """
        self.values = values
        if not self._are_changing_all_contracts(all_party_contracts) or self._has_different_emails(all_party_contracts):
            self._create_tickets()
            return
        if self._is_email_updated():
            self._update_email(all_party_contracts)
        elif self._is_bank_account_updated():
            self._update_bank_account(all_party_contracts)
