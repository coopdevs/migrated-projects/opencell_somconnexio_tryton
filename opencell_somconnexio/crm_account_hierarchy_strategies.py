import logging

from pyopencell.resources.customer import Customer
from pyopencell.exceptions import PyOpenCellAPIException, PyOpenCellHTTPException

logger = logging.getLogger(__name__)


class CRMAccountHierarchyStrategies():
    def __init__(self, contract):
        self.contract = contract
        try:
            self.customer = Customer.get(self.contract.party.id).customer
        except (PyOpenCellAPIException, PyOpenCellHTTPException):
            self.customer = None

    def strategies(self):
        if not self._customer_exists():
            return 'customer_hierarchy', {"crm_account_hierarchy_code": self._customer_account_code}

        if self._number_customer_accounts() < 1:
            return 'fallback', {}

        for customer_account in self._customer_accounts:
            if self._same_email(customer_account) and self._same_iban(customer_account):
                return 'subscription', {"crm_account_hierarchy_code": customer_account["code"]}

        return 'customer_account_hierarchy', {"crm_account_hierarchy_code": self._customer_account_code}

    @property
    def _customer_accounts(self):
        return self.customer.customerAccounts['customerAccount']

    @property
    def _customer_account_code(self):
        if not self.customer:
            return "{}_0".format(self.contract.party.id)

        customer_accounts_count = len(self.customer.customerAccounts['customerAccount'])
        return "{}_{}".format(self.customer.code, customer_accounts_count)

    def _customer_exists(self):
        return bool(self.customer)

    def _number_customer_accounts(self):
        return len(self._customer_accounts)

    def _same_email(self, customer_account):
        return customer_account['contactInformation']['email'] == self.contract.contact_email.value

    def _same_iban(self, customer_account):
        customer_iban = customer_account['methodOfPayment'][0]['bankCoordinates']['iban'].upper()
        # TODO: This code is duplicate in CRMAccountHierarchyFromContract class.
        # We can to move it to Contract object
        contract_iban = self.contract.receivable_bank_account.numbers[0].number_compact.upper()
        return customer_iban == contract_iban
